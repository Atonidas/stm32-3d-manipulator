#ifndef __ARM_BASE_API_H
#define __ARM_BASE_API_H

#include "main.h"
#include "draw_api.h"
#include "MyUart.h"
#include "adc.h"
#include "tim.h"
#include "main.h"
#include "nrf24L01.h"
#include "string.h"

#define V_BAT_K 0.004829844
#define I_ARM_K 0.0009706562

#define SI24_OK 1
#define SI24_IDLE 0

#define getVBAT(adc) (adc * V_BAT_K)
#define getIARM(adc) (adc * I_ARM_K)
#define BEEP_OFF() __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0)
#define getTouch() !HAL_GPIO_ReadPin(TOUCH_GPIO_Port, TOUCH_Pin)
#define PWR_ON() HAL_GPIO_WritePin(PWR_ON_GPIO_Port, PWR_ON_Pin, 1)
#define PWR_OFF() HAL_GPIO_WritePin(PWR_ON_GPIO_Port, PWR_ON_Pin, 0)

extern uint16_t adcValue[102];
extern uint32_t adcAvg[2];

void setBeep(uint8_t level);
void Music_Start(void);
void Music_Stop(void);
void Music_DD(uint8_t level);
#endif