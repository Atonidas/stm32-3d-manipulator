#include "arm_base_api.h"
#include "FreeRTOS.h"
#include "cmsis_os.h"


uint16_t BeepTemp[] = {3816, 3401, 3030, 2865, 2551, 2272, 2024, 1620, 1295, 1036, 829, 663, 530, 424, 340, 272, 218, 174, 140};
uint16_t adcValue[102] = {0};
uint32_t adcAvg[2] = {0};

void setBeep(uint8_t level)
{
    __HAL_TIM_SET_AUTORELOAD(&htim1, BeepTemp[level]);
    __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, BeepTemp[level] * 0.5);
}

void Music_Start(void)
{
    setBeep(15);
    osDelay(10);
    BEEP_OFF();
    osDelay(120);
    setBeep(15);
    osDelay(10);
    BEEP_OFF();
    osDelay(120);
    setBeep(17);
    osDelay(10);
    BEEP_OFF();
}

void Music_Stop(void)
{
    setBeep(15);
    osDelay(20);
    BEEP_OFF();
    osDelay(120);
    setBeep(15);
    osDelay(10);
    BEEP_OFF();
    osDelay(120);
    setBeep(13);
    osDelay(10);
    BEEP_OFF();
}

void Music_DD(uint8_t level)
{
    setBeep(level);
    osDelay(50);
    BEEP_OFF();
    osDelay(100);
    setBeep(level);
    osDelay(100);
    BEEP_OFF();
}