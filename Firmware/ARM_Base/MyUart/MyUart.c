#include "MyUart.h"
#include <stdio.h>
#include <stdarg.h>

volatile uint8_t rx_len = 0;               //接收一帧数据的长度
volatile uint8_t recv_end_flag = 0; //一帧数据接收完成标志
uint8_t volatile rx_buffer[100] = {0};     //接收数据缓存数组

void MyUart_Iint(void)
{
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
    HAL_UART_Receive_DMA(&huart1, rx_buffer, BUFFER_SIZE);
}
#define BUF_SIZE 64

void set_Print_Style(uint8_t color, uint8_t style)
{
    Uart1_Printf("\033[%dm", color, style);
}

void Uart1_Printf(char *fmt, ...)
{
    uint8_t Uart_Printf_Buf[BUF_SIZE] = {0};
    va_list args;
    va_start(args, fmt);
    vsnprintf((char *)Uart_Printf_Buf, BUF_SIZE, (char *)fmt, args);
    va_end(args);
    HAL_UART_Transmit(&huart1, Uart_Printf_Buf, sizeof(Uart_Printf_Buf), 0xff);
}

void Uart2_Printf(char *fmt, ...)
{
    uint8_t Uart_Printf_Buf[BUF_SIZE] = {0};
    va_list args;
    va_start(args, fmt);
    vsnprintf((char *)Uart_Printf_Buf, BUF_SIZE, (char *)fmt, args);
    va_end(args);
    HAL_UART_Transmit(&huart2, Uart_Printf_Buf, sizeof(Uart_Printf_Buf), 0xff);
}

void MyUart1_DMA_task(void)
{
    uint32_t tmp_flag = 0;
    uint32_t temp;
    tmp_flag = __HAL_UART_GET_FLAG(&huart1, UART_FLAG_IDLE); //获取IDLE标志位
    if ((tmp_flag != RESET))                                 // idle标志被置位
    {
        __HAL_UART_CLEAR_IDLEFLAG(&huart1);            //清除标志位
        HAL_UART_DMAStop(&huart1);                     //  停止DMA传输，防止
        temp = __HAL_DMA_GET_COUNTER(&hdma_usart1_rx); // 获取DMA中未传输的数据个数
        rx_len = BUFFER_SIZE - temp;                   //总计数减去未传输的数据个数，得到已经接收的数据个数
        recv_end_flag = 1;                             // 接受完成标志位置1
    }
    HAL_UART_IRQHandler(&huart1);
}

// void MyUart2_DMA_task(void)
// {
//     uint32_t tmp_flag2 = 0;
//     uint32_t temp;
//     tmp_flag2 = __HAL_UART_GET_FLAG(&huart2, UART_FLAG_IDLE); //获取IDLE标志位
//     if ((tmp_flag2 != RESET))                                 // idle标志被置位
//     {
//         __HAL_UART_CLEAR_IDLEFLAG(&huart2);            //清除标志位
//         HAL_UART_DMAStop(&huart2);                     //  停止DMA传输，防止
//         temp = __HAL_DMA_GET_COUNTER(&hdma_usart2_rx); // 获取DMA中未传输的数据个数
//         rx_len2 = BUFFER_SIZE - temp;                  //总计数减去未传输的数据个数，得到已经接收的数据个数
//         recv_end_flag2 = 1;                            // 接受完成标志位置1
//     }
//     HAL_UART_IRQHandler(&huart2);
// }