#include "esp8266.h"
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"


void ESP8266_Init(void)
{
    Uart1_Printf(ESP_MODE_STA);
    osDelay(2000);
    Uart1_Printf(ESP_RESTART);
    osDelay(2000);
    Uart1_Printf("AT+CWJAP=\"mi11\",\"123456789\"\r\n");
    osDelay(10000);
    Uart1_Printf(ESP_SINGLE_LINK);
    osDelay(2000);
    Uart1_Printf("AT+CIPSTART=\"TCP\",\"192.168.113.166\",9999\r\n");
    osDelay(10000);
    Uart1_Printf(ESP_CIPMODE);
    osDelay(2000);
    Uart1_Printf(ESP_CIPSEND);
    osDelay(2000);
}
