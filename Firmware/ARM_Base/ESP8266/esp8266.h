#include "MyUart.h"

#define ESP_UART huart1

#define ESP_RESTART "AT+RST\r\n"
#define ESP_MODE_STA "AT+CWMODE=1\r\n"
#define ESP_WIFI "AT+CWJAP="
#define ESP_SINGLE_LINK "AT+CIPMUX=0\r\n"
#define ESP_TCP_LINK "AT+CIPSTART=\"TCP\","
#define ESP_CIPMODE "AT+CIPMODE=1\r\n"
#define ESP_CIPSEND "AT+CIPSEND\r\n"

#define WIFI_NAME "\"zf\","
#define WIFI_PASSWORD "\"Zf990404\"\r\n"
#define TCP_ADDR "\"172.20.10.2\""
#define TCP_PORT "\"8888\"\r\n"

void ESP8266_Init(void);