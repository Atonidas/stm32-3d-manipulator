/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "arm_base_api.h"
#include "esp8266.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
uint8_t flag_SI24 = SI24_IDLE;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for myTask01 */
osThreadId_t myTask01Handle;
const osThreadAttr_t myTask01_attributes = {
    .name = "myTask01",
    .stack_size = 128 * 4,
    .priority = (osPriority_t)osPriorityNormal,
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
const osThreadAttr_t myTask02_attributes = {
    .name = "myTask02",
    .stack_size = 64 * 4,
    .priority = (osPriority_t)osPriorityLow,
};
/* Definitions for myTask03 */
osThreadId_t myTask03Handle;
const osThreadAttr_t myTask03_attributes = {
    .name = "myTask03",
    .stack_size = 192 * 4,
    .priority = (osPriority_t)osPriorityLow,
};
/* Definitions for myTask04 */
osThreadId_t myTask04Handle;
const osThreadAttr_t myTask04_attributes = {
    .name = "myTask04",
    .stack_size = 128 * 4,
    .priority = (osPriority_t)osPriorityLow,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void Task_Sys(void *argument);
void Task_Report(void *argument);
void Task_Sensor(void *argument);
void Task_Display(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void)
{
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of myTask01 */
  myTask01Handle = osThreadNew(Task_Sys, NULL, &myTask01_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(Task_Report, NULL, &myTask02_attributes);

  /* creation of myTask03 */
  myTask03Handle = osThreadNew(Task_Sensor, NULL, &myTask03_attributes);

  /* creation of myTask04 */
  myTask04Handle = osThreadNew(Task_Display, NULL, &myTask04_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */
}

/* USER CODE BEGIN Header_Task_Sys */
/**
 * @brief  Function implementing the myTask01 thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_Sys */
void Task_Sys(void *argument)
{
  /* USER CODE BEGIN Task_Sys */
  // uint8_t TX_ADDRESS[TX_ADR_WIDTH] = {0x34, 0x43, 0x10, 0x10, 0x01};
  uint8_t buf[32] = {0}, uart_buf[50] = {0};
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  uint8_t statePWR = 0, tick = 0;
  while (NRF24L01_Check())
  {
    osDelay(50);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  }
  ESP8266_Init();

  NRF24L01_RX_Mode(RX_ADDRESS);
  NRF24L01_RxPacket(buf);
  Uart1_Printf("开机！\n");
  PWR_OFF();
  Music_DD(8);
  MyUart_Iint();
  /* Infinite loop */
  for (;;)
  {
    if (getTouch())
    {
      osDelay(50);
      if (getTouch())
      {
        statePWR = !statePWR;
        if (statePWR)
        {
          Music_Start();
          PWR_ON();
        }
        else
        {
          Music_Stop();
          PWR_OFF();
        }
        osDelay(200);
      }
    }
    if (flag_SI24 == SI24_OK)
    {
      tick = 0;
      setBeep(15);
      osDelay(100);
      BEEP_OFF();
      flag_SI24 = SI24_IDLE;
    }
    if (tick++ > 500)
    {
      NRF24L01_RX_Mode(RX_ADDRESS);
      NRF24L01_RxPacket(buf);
    }
    if (recv_end_flag == 1) //接收完成标志
    {
      Uart2_Printf("%s", rx_buffer);
      recv_end_flag = 0; //清除接收结束标志位
      memset(rx_buffer, 0, rx_len);
      rx_len = 0;                                            //清除计数
      HAL_UART_Receive_DMA(&huart1, rx_buffer, BUFFER_SIZE); //重新打开DMA接收
    }
    osDelay(10);
  }
  /* USER CODE END Task_Sys */
}

/* USER CODE BEGIN Header_Task_Report */
/**
 * @brief Function implementing the myTask02 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_Report */
void Task_Report(void *argument)
{
  /* USER CODE BEGIN Task_Report */
  // Uart1_Printf("sys start!!");
  /* Infinite loop */
  for (;;)
  {
    // Uart1_Printf("adc 0:%d, adc 1:%d    ", adcAvg[0], adcAvg[1]);
    // Uart1_Printf("I:%dmA  BAT:%d.%dV\n", getIARM(adcAvg[0]), getVBAT(adcAvg[1]) / 1000, getVBAT(adcAvg[1]) % 1000);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    osDelay(500);
  }
  /* USER CODE END Task_Report */
}

/* USER CODE BEGIN Header_Task_Sensor */
/**
 * @brief Function implementing the myTask03 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_Sensor */
void Task_Sensor(void *argument)
{
  /* USER CODE BEGIN Task_Sensor */
  HAL_TIM_Base_Start(&htim3);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adcValue, 100);
  /* Infinite loop */
  for (;;)
  {
    // ClearScreen();
    adcAvg[0] = 0;
    adcAvg[1] = 0;
    for (int i = 0; i < 82; i += 2)
    {
      adcAvg[0] += adcValue[i];
      adcAvg[1] += adcValue[i + 1];
    }
    adcAvg[0] = adcAvg[0] / 40;
    adcAvg[1] = adcAvg[1] / 40;

    // adcAvg[0] = adcValue[0];
    // adcAvg[1] = adcValue[1];
    PrintString(4, 32, "%2d.%d%d A", (int)getIARM(adcAvg[0]), (int)(getIARM(adcAvg[0]) * 10) % 10, (int)(getIARM(adcAvg[0]) * 100) % 10);
    PrintString(6, 32, "%2d.%d%d V", (int)getVBAT(adcAvg[1]), (int)(getVBAT(adcAvg[1]) * 10) % 10, (int)(getVBAT(adcAvg[1]) * 100) % 10);
    osDelay(20);
  }

  /* USER CODE END Task_Sensor */
}

/* USER CODE BEGIN Header_Task_Display */
/**
 * @brief Function implementing the myTask04 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_Display */
void Task_Display(void *argument)
{
  /* USER CODE BEGIN Task_Display */
  // InitGraph();
  // PrintString(0, 0, "HELLO");
  /* Infinite loop */
  for (;;)
  {
    UpdateScreen();
    osDelay(500);
  }
  /* USER CODE END Task_Display */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  uint8_t buf[50] = {0};
  if (GPIO_Pin == SI24_IRQ_Pin)
  {
    flag_SI24 = SI24_OK;
    // while(!HAL_GPIO_WritePin(SI24_CS_GPIO_Port, SI24_CS_Pin)) osDelay(1);
    NRF24L01_RxPacket(buf);
    PrintString(0, 0, "                  ");
    PrintString(0, 0, buf);
    Uart2_Printf("%s", buf);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  }
}
/* USER CODE END Application */
