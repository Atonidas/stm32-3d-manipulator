#define LOG_TAG "Process"

#include "tim.h"

#include "TimTask.h"
#include "MY_Process.h"

#include "usart.h"
#include "adc.h"
#include "draw_api.h"
#include "elog.h"
#include "StepMotor.h"

uint16_t adcValue[50] = {0};
uint32_t adcAvg = 0;
uint8_t ledState = 0x05, beepState = 0x01, beepNum = 12;
uint16_t BeepTemp[12] = {3816, 3401, 3030, 2865, 2551, 2272, 2024, 960, 640, 320, 160, 80};

void Task_OLED_Upadata(void)
{
    UpdateScreen();
}

void Task_UART_Print(void)
{
    log_i("sys working...");
    Uart1_Printf("sys working...\n");
}

void Task_LED(void)
{
    static unsigned char stateTimer = 0;
    stateTimer = (stateTimer + 1) & 7;

    __HAL_TIM_SetAutoreload(&htim3, BeepTemp[beepNum - 1]);

    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, (((0xff - ledState) >> stateTimer) & 1));
    __HAL_TIM_SetCompare(&htim3, TIM_CHANNEL_1, ((beepState >> stateTimer) & 1) * (BeepTemp[beepNum - 1] * 0.5));
}

void Task_ADC(void)
{
    adcAvg = 0;
    for (int i = 0; i < 50; i++)
        adcAvg += adcValue[i];
    adcAvg = adcAvg / 50;
    PrintString(0, 0, "moto : %dma", (int)(adcAvg * 0.257));
}

void Process_Init()
{
    elog_init();
    elog_start();
    elog_set_text_color_enabled(true);

    HAL_ADC_Start_DMA(&hadc1, adcValue, 50);
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);

    InitGraph();

    Uart1_Printf("sys start!\n");
    log_i("USB connect!");

    HAL_TIM_Base_Start_IT(&TASK_TIM);
}