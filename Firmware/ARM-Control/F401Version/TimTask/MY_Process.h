#ifndef MY_PROCESS_H
#define MY_PROCESS_H

#include "main.h"

void Task_OLED_Upadata(void);
void Task_UART_Print(void);
void Task_LED(void);
void Task_ADC(void);

void Process_Init();
#endif

