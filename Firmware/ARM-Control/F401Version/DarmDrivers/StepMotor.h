#ifndef __STEPMOTOR_H__
#define __STEPMOTOR_H__

#include "main.h"

typedef struct __MotorInfo
{
    uint8_t DirToggle;
    uint8_t DirPin;
    uint8_t StpPin;
    uint16_t Speed;
    uint32_t TickTemp;
    int TotalPulse;
    float MotoAngle;
} MotorInfo;

void moveOneFrame(int *MoveFrame);

#endif