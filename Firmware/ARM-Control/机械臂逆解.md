**机械臂逆解**

# 参数测定

## 长度信息

- 大臂长 200mm
- 小臂长 215mm
- 底座高 115mm
- 臂向补偿 90mm

## 传动信息

- 齿轮比 6.75
- 输出参数 21600/r

## 基准信息

- 大臂归零角度
- 小臂归零角度

# 理论计算

> 目的：输入**空间坐标（x, y, z）**，计算出三个电机的**角度(α, β, γ)**

## 底座角 Θ

$\tan(γ) = \frac{x}{y}$

$\angleγ = atan\frac{x}{y}$

## 垂直投影

$垂直投影 = 完整投影 - 臂向补偿$

$垂直投影 = \sqrt{(x^2 + y^2)} - 臂向补偿$

## 大臂角

### 底座高度 > z

![](pic/%E5%A4%A7%E8%87%82%E8%A7%92%E7%A4%BA%E6%84%8F.bmp)

$短边 = 底座高度 - z$

$斜边 = \sqrt{短边^2 + 垂直投影^2}$

$\angle1 = acos\frac{大臂长度^2 + 斜边长度^2 - 小臂长度^2}{2 * 大臂长度 *斜边长度 }$

$\angle2 = atan\frac{垂直投影}{短边}$

$\angle大臂角 = \angle1 + \angle2 - 90°$

### 底座高度 < z

![](pic/%E5%A4%A7%E8%87%82%E8%A7%92%E7%A4%BA%E6%84%8F2.bmp)

$短边 = z - 底座高度$

$斜边 = \sqrt{短边^2 + 垂直投影^2}$

$\angle1 = acos\frac{大臂长度^2 + 斜边长度^2 - 小臂长度^2}{2 * 大臂长度 *斜边长度 }$

$\angle2 = atan\frac{短边}{垂直投影}$

$\angle大臂角 = \angle1 + \angle2$

### 底座高度 = z

![](pic/%E5%A4%A7%E8%87%82%E8%A7%92%E7%A4%BA%E6%84%8F3.bmp)

$短边 = 0$

$斜边 = \sqrt{短边^2 + 垂直投影^2}$

$\angle1 = acos\frac{大臂长度^2 + 斜边长度^2 - 小臂长度^2}{2 * 大臂长度 *斜边长度 }$

$\angle2 = 0$

$\angle大臂角 = \angle1 + \angle2$

## 小臂角

![](pic/%E5%B0%8F%E8%87%82%E8%A7%92%E7%A4%BA%E6%84%8F.bmp)

$\angle小臂角 = acos\frac{大臂长度^2  + 小臂长度^2 - 斜边长度^2}{2 * 大臂长度 * 小臂长度} + \angle大臂角 - 90°$

## 代码整合

```c
#include <math.h>
#define BASE_H      115
#define MAIN_L      200
#define SECOND_L    215
#define PI  3.14159265758
void get_MianAngle(int x, y, z)
{
    float project_V, short_L, hypotenuse_L;
    float angle_1, angle_2, angle_z, angle_main, angle_second;
    angle_z = angle(x/y);
    project_V = sqrt(x * x + y * y);
    short_L = abs(BASE_H - z);
    hypotenuse_L = sqrt(short_L * short_L + project_V * project_V);
    angle_1 = acos((MAIN_L * MAIN_L + hypotenuse_L * hypotenuse_L - SECOND_L * SECOND_L) / (2 * MAIN_L * hypotenuse_L));\
    if(z == BASE_H)
        angle_2 = 0;
    else if(z < BASE_H)
        angle_2 = atan(project_V / short_L) - PI/2;
    else if(z > BASE_H)
        angle_2 = atan( / project_V);
    angle_main = angle_1 + angle_2;
    float project_V, short_L, hypotenuse_L;
    angle_second = acos((MAIN_L * MAIN_L + SECOND * SECOND - hypotenuse_L * hypotenuse_L) / (2 * MAIN_L * SECOND_L)) + angle_mian - PI / 2;
    //TODO: 机械臂控制(angle_z, angle_main, angle_second);
}


```
