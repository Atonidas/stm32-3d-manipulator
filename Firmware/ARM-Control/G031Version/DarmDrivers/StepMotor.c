#include "StepMotor.h"
#include "string.h"
#include "spi.h"
#include "usart.h"

#define MOTOR_SPI hspi1
#define MOTOR_NUM 3
#define LINE_CLOCK 125000

#define BUF_LENGTH 1000

static uint8_t MotorBuf_Out[BUF_LENGTH] = {0};
static uint8_t MotorBuf_Temp[BUF_LENGTH] = {0};

MotorInfo ArmMotor[3] = {
    {0, 0x04, 0x08, 60, 0, 0, 0},
    {0, 0x10, 0x40, 60, 0, 0, 0},
    {0, 0x20, 0x80, 60, 0, 0, 0},
};

void motorInit(void)
{

}

void moveOneFrame(int *MoveFrame)//输入一帧动作的脉冲，计算并填入缓冲区
{
    uint8_t DirTemp = 0, Point = 0;
    uint16_t Num = 0;
    memcpy(MotorBuf_Out, MotorBuf_Temp, BUF_LENGTH);
    HAL_SPI_Transmit_DMA(&MOTOR_SPI, MotorBuf_Out, BUF_LENGTH);

    for (; Num < MOTOR_NUM; Num++)
        DirTemp += ArmMotor[Num].DirPin * (ArmMotor[Num].DirToggle == (MoveFrame[Num] > 0));
    memset(MotorBuf_Temp, DirTemp, BUF_LENGTH);

    for (Num = 0; Num < MOTOR_NUM; Num++)
    {
        Point = BUF_LENGTH / abs(MoveFrame[Num]);
        for(uint16_t i = 0; i < BUF_LENGTH; i += Point)
            MotorBuf_Temp[i] += ArmMotor[Num].StpPin;
    }
}
