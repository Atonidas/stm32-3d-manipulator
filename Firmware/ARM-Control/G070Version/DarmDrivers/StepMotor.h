#ifndef __STEPMotor__H__
#define __STEPMotor__H__

#include "main.h"
extern volatile uint16_t motorSpeed;

#define MOTOR_NUM 3
#define MOROT_PER 60
#define MEMORY_LEN 500
#define BUF_LENGTH 500 // 16ms

typedef struct __Motor_Info
{
    uint8_t DirToggle;
    uint8_t DirPin;
    uint8_t StpPin;
    int TotalPulse;
    float MotoAngle;
} MotorInfo;

typedef struct __Motor_Data_Tydef
{
    int16_t L;
    int16_t R;
    int16_t Z;
} __MotorData_Tydef;

typedef struct __Motor_Angle_Tydef
{
    double L;
    double R;
    double Z;
} __MotorAngle_Tydef;

typedef struct __Point_Data_Tydef
{
    double X;
    double Y;
    double Z;
} __PonitData_Tydef;


#define IT_HAFT 1
#define IT_END 0

extern MotorInfo ArmMotor[];

void Motor_Zero(void);
void Motor_Print(void);
void Motor_Angle(__MotorAngle_Tydef TargetAngle);
void Motor_Loading(int l, int r, int z, uint8_t num);
void Motor_Start(void);
void Motor_Init(void);
void Motor_Test(int l, int r, int z);
void Move_OneFrame(__MotorData_Tydef dataTemp, uint8_t IT_type);
void Motor_Process(uint8_t mode);

void getArmAngle(int x, int y, int z, __MotorAngle_Tydef *Angle);
void getArmPoint(int l, int r, int z, __PonitData_Tydef *Ponit);
void Frame_By_Frame(__PonitData_Tydef Start, __PonitData_Tydef End, int speed);
#endif