#include "StepMotor.h"
#include "string.h"
#include "spi.h"
#include "MyUart.h"
#include "queue.h"
#include "TimTask.h"
#define MOTOR_SPI hspi2

volatile uint8_t queueFlag = 0;
volatile uint16_t motorSpeed = 300;
__MotorData_Tydef motoData_temp;
Queue motorQueue_L, motorQueue_R, motorQueue_Z;

static uint8_t MotorBuf_Out[BUF_LENGTH * 2] = {0};

MotorInfo ArmMotor[3] = {
    {0, 0x02, 0x04, 0, 0},
    {1, 0x08, 0x10, 0, 0},
    {1, 0x80, 0x40, 0, 0},
};

void Motor_QueueIn(__MotorData_Tydef dataTemp)
{
    EnQueue(&motorQueue_L, &dataTemp.L);
    EnQueue(&motorQueue_R, &dataTemp.R);
    EnQueue(&motorQueue_Z, &dataTemp.Z);
}

uint8_t Motor_QueueOut(__MotorData_Tydef *dataTemp)
{
    if (QueueIsEmpty(&motorQueue_L) && QueueIsEmpty(&motorQueue_R) && QueueIsEmpty(&motorQueue_Z))
        return 0;
    else
    {
        DeQueue(&motorQueue_L, &dataTemp->L);
        DeQueue(&motorQueue_R, &dataTemp->R);
        DeQueue(&motorQueue_Z, &dataTemp->Z);
        return 1;
    }
}

void Motor_Print(void)
{
    Uart1_Printf("L: %8d\nR: %8d\nZ: %8d\n", ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse);
}

void Motor_Loading(int l, int r, int z, uint8_t num)
{
    motoData_temp.L = l;
    motoData_temp.R = r;
    motoData_temp.Z = z;
    for (; num >= 1; num--)
    {
        Motor_QueueIn(motoData_temp);
    }
}
void Motor_Start(void)
{
    Move_OneFrame(motoData_temp, IT_HAFT);
    HAL_SPI_Transmit_DMA(&hspi2, MotorBuf_Out, BUF_LENGTH * 2);
}
void Motor_Test(int l, int r, int z)
{
    Motor_Loading(l, r, z, 1);
    Motor_Start();
}

void Motor_Zero(void)
{
    Motor_Start();
    Motor_Loading(30, 60, 0, 20);
    while (queueFlag == 0)
        HAL_Delay(5);

    while (getLimt_L())
    {
        if (queueFlag)
        {
            Motor_Loading(-20, 0, 0, 1);
            Motor_Start();
            queueFlag = 0;
        }
        // HAL_Delay(1);
    }
    while (getLimt_R())
    {
        if (queueFlag)
        {
            Motor_Loading(0, -20, 0, 1);
            Motor_Start();
            queueFlag = 0;
        }
        // HAL_Delay(1);
    }
    Motor_Loading(42, 74, 0, 49);
    Motor_Start();
    while (queueFlag == 0)
        HAL_Delay(5);
    ArmMotor[0].TotalPulse = -2700;
    ArmMotor[1].TotalPulse = 0;
}

void Motor_Angle(__MotorAngle_Tydef TargetAngle)
{
    int target_L, target_R, target_Z;
    int length_L, length_R, length_Z;
    int oneper_L, oneper_R, oneper_Z;
    int max_length;
    uint8_t frame;

    target_L = (TargetAngle.L * MOROT_PER);
    target_R = (TargetAngle.R * MOROT_PER);
    target_Z = (TargetAngle.Z * MOROT_PER);
    length_L = target_L - ArmMotor[0].TotalPulse;
    length_R = target_R - ArmMotor[1].TotalPulse;
    length_Z = target_Z - ArmMotor[2].TotalPulse;

    max_length = abs(length_L) > abs(length_R) ? abs(length_L) : abs(length_R);
    max_length = max_length > abs(length_Z) ? max_length : abs(length_Z);
    frame = max_length / motorSpeed;
    oneper_L = length_L / frame;
    oneper_R = length_R / frame;
    oneper_Z = length_Z / frame;
    Motor_Loading(oneper_L, oneper_R, oneper_Z, frame);
}

/* 定义全局数组用于保存队列的内部空间 */
Node items[3][MEMORY_LEN + 1];
void Motor_Init(void)
{
    InitializeQueue(&motorQueue_L, items[0], sizeof(items[0]) / sizeof(Node));
    InitializeQueue(&motorQueue_R, items[1], sizeof(items[1]) / sizeof(Node));
    InitializeQueue(&motorQueue_Z, items[2], sizeof(items[2]) / sizeof(Node));
    Motor_Loading(0, 0, 0, 1);
    Motor_Zero();
}

void Motor_Process(uint8_t mode)
{
    if (Motor_QueueOut(&motoData_temp) == 0)
    {
        HAL_SPI_DMAStop(&hspi2);
        queueFlag = 1;
        memset(MotorBuf_Out, 0, BUF_LENGTH * 2);
    }
    else
        Move_OneFrame(motoData_temp, mode);
}

/**
 * @brief  根据传入的动作指令填充半帧数据
 * @param  dataTemp 传入的动作指令结构体
 * @param  IT_type 填充的是前半还是后半
 */
void Move_OneFrame(__MotorData_Tydef dataTemp, uint8_t IT_type)
{
    uint8_t DirTemp = 0, Point = 0, PointTemp, Num = 0;
    int32_t oneFrame[] = {dataTemp.L, dataTemp.R, dataTemp.Z};
    int32_t Start = (IT_type == IT_END) ? BUF_LENGTH : 0;
    int16_t tickNums = 0, prr = 0;

    ArmMotor[0].TotalPulse += dataTemp.L;
    ArmMotor[1].TotalPulse += dataTemp.R;
    ArmMotor[2].TotalPulse += dataTemp.Z;
    // Uart1_Printf("move(%d, %d, %d)\n", dataTemp.L, dataTemp.R, dataTemp.Z);
    for (Num = 0; Num < MOTOR_NUM; Num++)
        DirTemp += ArmMotor[Num].DirPin * (ArmMotor[Num].DirToggle == (oneFrame[Num] > 0));
    memset(MotorBuf_Out + Start, DirTemp, BUF_LENGTH);
    for (Num = 0; Num < MOTOR_NUM; Num++)
    {
        if (oneFrame[Num] != 0)
        {
            Point = BUF_LENGTH / abs(oneFrame[Num]);
            PointTemp = Point + 1;
            tickNums = abs(oneFrame[Num]);

            for (uint16_t i = 0; i < tickNums; i++)
            {
                if (BUF_LENGTH % tickNums == 0)
                {
                    prr = tickNums;
                    break;
                }
                if (i * Point + (tickNums - i) * (Point + 1) == BUF_LENGTH)
                {
                    prr = i;
                    break;
                }
            }
            tickNums = 0;
            for (uint16_t i = 0; i < BUF_LENGTH; i += Point)
            {
                if (tickNums++ >= prr)
                    Point = PointTemp;
                MotorBuf_Out[i + Start] += ArmMotor[Num].StpPin;
            }
        }
    }
}
