#include "math.h"
#include "MyUart.h"
#include "StepMotor.h"

#define COMPENSATE 115

#define ANGLE_FIX 0.0583333f
#define BASE_H 65
#define MAIN_L 200
#define SECOND_L 215

#define PI 3.14159265758
#define ANGLE_P 0.00029088821f
#define toAngle(A) (A * 180 / PI)

#define FPS 60

void getArmPoint(int l, int r, int z, __PonitData_Tydef *Ponit)
{
    double Al, Ar, Az;
    double Shadow_H, Shadow_V;

    Al = l * ANGLE_P;
    Ar = r * ANGLE_P;
    Az = z * ANGLE_P;
    Al = (Al > 0) ? Al : -Al;
    Shadow_H = MAIN_L * sin(Ar) + SECOND_L * sin(Al) + COMPENSATE;
    Shadow_V = MAIN_L * cos(Ar) - SECOND_L * cos(Al);

    Ponit->X = (double)(Shadow_H * sin(Az));
    Ponit->Y = (double)(Shadow_H * cos(Az));
    Ponit->Z = (double)(BASE_H + Shadow_V);

    // Uart1_Printf("Shadow_H:%d, Shadow_V:%d\n", (int)Shadow_H, (int)Shadow_V);
    // Uart1_Printf("Angle: (%d, %d, %d)\n", (int)toAngle(Al), (int)toAngle(Ar), (int)toAngle(Az));
}

void getArmAngle(int x, int y, int z, __MotorAngle_Tydef *Angle)
{
    float project_V, short_L, hypotenuse_L;
    float angle_1, angle_2;
    Angle->Z = atan2(x, y);
    project_V = sqrt(x * x + y * y) - COMPENSATE;
    short_L = abs(BASE_H - z);
    hypotenuse_L = sqrt(short_L * short_L + project_V * project_V);
    angle_1 = acos((MAIN_L * MAIN_L + hypotenuse_L * hypotenuse_L - SECOND_L * SECOND_L) / (2 * MAIN_L * hypotenuse_L));
    if (z == BASE_H)
        angle_2 = 0;
    else if (z < BASE_H)
        angle_2 = atan(project_V / short_L) - PI / 2;
    else if (z > BASE_H)
        angle_2 = atan(short_L / project_V);

    Angle->R = PI / 2 - angle_1 - angle_2;
    Angle->L = -acos((MAIN_L * MAIN_L + SECOND_L * SECOND_L - hypotenuse_L * hypotenuse_L) / (2 * MAIN_L * SECOND_L)) + Angle->R + ANGLE_FIX;

    Angle->L = Angle->L * 180 / PI;
    Angle->R = Angle->R * 180 / PI;
    Angle->Z = Angle->Z * 180 / PI;
    // Motor_Angle(&Angle);
}

void Frame_By_Frame(__PonitData_Tydef Start, __PonitData_Tydef End, int speed)
{
    __PonitData_Tydef Vector, pNext, Per;
    __MotorAngle_Tydef angleNow, angleNext;
    double Length;
    int stepL, stepR, stepZ;
    uint8_t Frame;

    Vector.X = End.X - Start.X;
    Vector.Y = End.Y - Start.Y;
    Vector.Z = End.Z - Start.Z;
    Length = sqrt(Vector.X * Vector.X + Vector.Y * Vector.Y + Vector.Z * Vector.Z);
    Per.X = speed * Vector.X / Length;
    Per.Y = speed * Vector.Y / Length;
    Per.Z = speed * Vector.Z / Length;
    Frame = Length / speed * FPS;
    // Uart1_Printf("Start:(%d, %d, %d)\n", (int)Start.X,(int)Start.Y,(int)Start.Z);
    // Uart1_Printf("End:(%d, %d, %d)\n", (int)End.X,(int)End.Y,(int)End.Z);
    // Uart1_Printf("Vector:(%d, %d, %d)\n", (int)Vector.X,(int)Vector.Y,(int)Vector.Z);
    // Uart1_Printf("100Per:(%d, %d, %d)\n", (int)(Per.X*100),(int)(Per.Y*100),(int)(Per.Z*100));
    // Uart1_Printf("Length:%d, Frame:%d\n", (int)Length, Frame);
    for (uint8_t i = 0; i < Frame; i++)
    {
        pNext.X = Start.X + Per.X / FPS;
        pNext.Y = Start.Y + Per.Y / FPS;
        pNext.Z = Start.Z + Per.Z / FPS;
        getArmAngle(Start.X, Start.Y, Start.Z, &angleNow);
        getArmAngle(pNext.X, pNext.Y, pNext.Z, &angleNext);
        stepL = (angleNext.L - angleNow.L) * MOROT_PER;
        stepR = (angleNext.R - angleNow.R) * MOROT_PER;
        stepZ = (angleNext.Z - angleNow.Z) * MOROT_PER;
        Motor_Loading(stepL, stepR, stepZ, 1);
        Start.X = pNext.X;
        Start.Y = pNext.Y;
        Start.Z = pNext.Z;
        // Uart1_Printf("(x, y, z) (per) : (%d, %d, %d) (%d, %d, %d)\n", (int)Start.X, (int)Start.Y, (int)Start.Z, stepL, stepR, stepZ);
    }
    pNext.X = End.X;
    pNext.Y = End.Y;
    pNext.Z = End.Z;
    getArmAngle(Start.X, Start.Y, Start.Z, &angleNow);
    getArmAngle(pNext.X, pNext.Y, pNext.Z, &angleNext);
    stepL = (angleNext.L - angleNow.L) * MOROT_PER;
    stepR = (angleNext.R - angleNow.R) * MOROT_PER;
    stepZ = (angleNext.Z - angleNow.Z) * MOROT_PER;
    Motor_Loading(stepL, stepR, stepZ, 1);
    Start.X = pNext.X;
    Start.Y = pNext.Y;
    Start.Z = pNext.Z;
    Motor_Start();
}