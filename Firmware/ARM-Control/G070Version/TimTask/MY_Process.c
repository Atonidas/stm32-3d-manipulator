#define LOG_TAG "Process"

#include "tim.h"

#include "TimTask.h"
#include "MY_Process.h"

#include "MyUart.h"
#include "adc.h"
#include "draw_api.h"
#include "StepMotor.h"
#include "MyUart.h"
#include "Shell_App.h"
#include "string.h"
#include "math.h"
#include "spi.h"
extern int motoData[3];
extern uint8_t DeviceState, recordPoint, recordNum, armState;
extern __PonitData_Tydef record_Data[10];
extern volatile uint8_t queueFlag;
volatile uint8_t RxPoint = 0, Rx_data2[100];
int16_t obj_X = 0, obj_Y = 0;
uint16_t adcValue[100] = {0};
uint32_t adcAvg[2] = {0};
uint8_t ledState = 0x05, beepState = 0x00, beepNum = 12, AiClawState = 0, objRefesh = 0;
uint8_t DataTemp[50] = {0}, DataLen = 0;
uint16_t BeepTemp[14] = {3816, 3401, 3030, 2865, 2551, 2272, 2024, 960, 640, 320, 160, 80, 40, 20};
uint16_t tick = 0, claw_Timer = 0;

void Task_OLED_Upadata(void)
{
    UpdateScreen();
}

void Task_UART_Print(void)
{
    char num;
    char *pTemp = NULL;
    if (recv_end_flag == 1) //接收完成标志
    {
        cmd_Analyze((char *)rx_buffer);
        recv_end_flag = 0; //清除接收结束标志位
        memset(rx_buffer, 0, rx_len);
        rx_len = 0;                                            //清除计数
        HAL_UART_Receive_DMA(&huart1, rx_buffer, BUFFER_SIZE); //重新打开DMA接收
    }
    if (recv_end_flag2 == 1) //接收完成标志
    {
        for (int i = 0; i < 10; i++)
        {
            if ((rx_buffer2[i] == 't') && (rx_buffer2[i + 1] == ' '))
                num = i;
        }
        if ((num > 0) && (num < 10))
        {
            pTemp = rx_buffer2 + num + 2;
            pTemp = strtok(pTemp, bank);
            obj_X = atoi(pTemp);
            pTemp = strtok(NULL, bank);
            obj_Y = atoi(pTemp);
            objRefesh = 1;
        }
        recv_end_flag2 = 0; //清除接收结束标志位
        memset(rx_buffer2, 0, rx_len2);
        rx_len2 = 0;                                            //清除计数
        HAL_UART_Receive_DMA(&huart2, rx_buffer2, BUFFER_SIZE); //重新打开DMA接收
    }
}

void Task_LED(void)
{
    static unsigned char stateTimer = 0;
    stateTimer = (stateTimer + 1) & 7;
    __HAL_TIM_SetAutoreload(&htim1, BeepTemp[beepNum - 1]);
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, (((0xff - ledState) >> stateTimer) & 1));
    __HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_2, ((beepState >> stateTimer) & 1) * (BeepTemp[beepNum - 1] * 0.5));
}

void Task_ADC(void)
{
    adcAvg[0] = 0, adcAvg[1] = 0;
    for (int i = 0; i < 100; i = i + 2)
    {
        adcAvg[0] += adcValue[i];
        adcAvg[1] += adcValue[i + 1];
    }
    adcAvg[0] = adcAvg[0] / 50;
    adcAvg[1] = adcAvg[1] / 50;
    SetFontSize(1);
    PrintString(0, 0, "adc1  %4d adc2  %4d", (int)(adcAvg[0]), (int)(adcAvg[1]));
    PrintString(0, 10, "Power %d.%dV", (int)(adcAvg[0] * 5.64 / 1000), (int)(adcAvg[0] * 5.64) % 1000 / 10);
    PrintString(0, 20, "moto  %dma", (int)(adcAvg[1] * 0.257));
    SetFontSize(0);
    PrintString(6, 0, "cube: %d,%d  ", obj_X, obj_Y);
}

void Task_Motor(void)
{
    static __PonitData_Tydef pNow;
    static int claw_tick = 0;
    if (claw_tick < claw_Timer)
    {
        claw_tick++;
        if (claw_Timer < claw_tick + 3)
        {
            HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 0);
            HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 0);
            Uart1_Printf("Time max\n");
            claw_tick = 1;
            claw_Timer = 0;
        }
        if (((int)(adcAvg[1] * 0.257) > 180) && (claw_tick > 100))
        {
            HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 0);
            HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 0);
            Uart1_Printf("I max\n");
            claw_tick = 1;
            claw_Timer = 0;
        }
    }
    if (armState == 1)
    {
        if (queueFlag)
        {
            queueFlag = 0;
            armState = 0;
            beepState = 0x00;
        }
    }
    if (DeviceState == RECORD_REPLAY)
    {
        if (queueFlag)
        {
            queueFlag = 0;
            if (recordPoint == recordNum)
            {
                beepNum = 9;
                beepState = 0x00;
                DeviceState == RECORD_IDLE;
                Uart1_Printf("replay over\n");
            }
            else if (recordPoint < recordNum)
            {
                HAL_Delay(500);
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
                Frame_By_Frame(pNow, record_Data[recordPoint], motorSpeed);
                Uart1_Printf("Replay @Point%d : (%d, %d, %d)\n", recordPoint, (int)record_Data[recordPoint].X, (int)record_Data[recordPoint].Y, (int)record_Data[recordPoint].Z);
                recordPoint++;
            }
        }
    }
    if (AiClawState > 0)
        Auto_Claw();
}

#define MOVE_START 1
#define MOVE_TO_H 2
#define MOVE_TO_L 3
#define MOVE_DOWN 4
#define MOVE_CLAWING 5
#define MOVE_WATING 6
void Auto_Claw(void)
{
    static __PonitData_Tydef pNow, pNext;
    static uint8_t lastState;
    static double det_X, det_Y;
    switch (AiClawState)
    {
    case MOVE_START:
        HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 1);
        HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 0);
        claw_Timer = 500;
        getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
        getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNext);
        pNext.Z = 120;
        Frame_By_Frame(pNow, pNext, motorSpeed);
        lastState = MOVE_START;
        AiClawState = MOVE_WATING;
        Uart1_Printf("to Z 120\n");
        break;
    case MOVE_TO_H:
        if (objRefesh)
        {
            objRefesh = 0;
            HAL_Delay(500);
            if (obj_X < -10)
            {
                Motor_Loading(0, 0, 30, 1);
                Motor_Start();
                lastState = MOVE_TO_H;
                AiClawState = MOVE_WATING;
                Uart1_Printf("Go right\n");
            }
            else if (obj_X > 10)
            {
                Motor_Loading(0, 0, -30, 1);
                Motor_Start();
                lastState = MOVE_TO_H;
                AiClawState = MOVE_WATING;
                Uart1_Printf("Go left\n");
            }
            else
            {
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
                det_X = pNow.X / sqrt(pNow.X * pNow.X + pNow.Y * pNow.Y);
                det_Y = pNow.Y / sqrt(pNow.X * pNow.X + pNow.Y * pNow.Y);
                // det_X = (det_X > 0) ? det_X : -det_X;
                // det_Y = (det_Y > 0) ? det_Y : -det_Y;
                AiClawState = MOVE_TO_L;
            }
        }
        break;
    case MOVE_TO_L:
        if (objRefesh)
        {
            objRefesh = 0;
            if (obj_Y < -68)
            {
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNext);
                pNext.X -= 3 * det_X;
                pNext.Y -= 3 * det_Y;
                Frame_By_Frame(pNow, pNext, motorSpeed);
                lastState = MOVE_TO_L;
                AiClawState = MOVE_WATING;
            }
            else if (obj_Y > -52)
            {
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
                getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNext);
                pNext.X += 3 * det_X;
                pNext.Y += 3 * det_Y;
                Frame_By_Frame(pNow, pNext, motorSpeed);
                lastState = MOVE_TO_L;
                AiClawState = MOVE_WATING;
            }
            else
                AiClawState = MOVE_DOWN;
        }
        break;
    case MOVE_DOWN:
        getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
        getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNext);
        pNext.Z = 10;
        Frame_By_Frame(pNow, pNext, motorSpeed / 2);
        lastState = MOVE_DOWN;
        AiClawState = MOVE_WATING;
        Uart1_Printf("Go Down\n");
        break;
    case MOVE_WATING:
        if (queueFlag)
        {
            queueFlag = 0;
            if (lastState == MOVE_START)
            {
                AiClawState = MOVE_TO_H;
            }
            else if (lastState == MOVE_DOWN)
            {
                HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 0);
                HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 1);
                HAL_Delay(500);
                claw_Timer = 3000;
                AiClawState = MOVE_CLAWING;
            }
            else
                AiClawState = lastState;
        }
        break;
    case MOVE_CLAWING:
        if (claw_Timer == 0)
        {
            getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
            getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNext);
            pNext.Z = 120;
            Frame_By_Frame(pNow, pNext, motorSpeed);
            AiClawState = 0;
        }
        break;
    default:
        break;
    }
}

void Process_Init()
{
    HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adcValue, 100);

    InitGraph();
    MyUart_Iint();
    HAL_GPIO_WritePin(MOTO_EN_GPIO_Port, MOTO_EN_Pin, 0);
    HAL_GPIO_WritePin(SRCLR_GPIO_Port, SRCLR_Pin, 1);
    Print_HelpTAG();
    Motor_Init();
    arm_register();
    HAL_TIM_Base_Start_IT(&htim15);              // ADC TIM
    HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2); // BEEP PWM
    HAL_TIM_Base_Start_IT(&TASK_TIM);
    Uart2_Printf("hello\n");
}

void HAL_SPI_TxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
    Motor_Process(IT_HAFT);
}
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
    Motor_Process(IT_END);
}

// void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
// {
//     if (huart == &huart2)
//     {
//         tick++;
//         rx_buffer2[tick] = rxByte;
//         if (tick > 50)
//             tick = 0;
//         if (rxByte == 'Z')
//         {
//             memset(DataTemp, 50, 0);
//             DataLen = tick;
//             memcpy(DataTemp, rx_buffer2, DataLen);
//             tick = 0;
//         }
//         HAL_UART_Receive_IT(&huart2, &rxByte, 1);
//     }
// }
