#include "TimTask.h"
#include "MY_Process.h"
#include "tim.h"

TASK_COMPONENTS TaskComps[TASKS_MAX] =
    {
        {0, 1, 33, Task_OLED_Upadata},
        {0, 2, 100, Task_UART_Print},
        {0, 3, 125, Task_LED},
        {0, 4, 20, Task_ADC},
        {0, 5, 10, Task_Motor}
};

void TaskRemarks(void)
{
  for (uint8_t i = 0; i < TASKS_MAX; i++)
  {
    if (TaskComps[i].Timer)
    {
      TaskComps[i].Timer--;
      if (TaskComps[i].Timer == 0)
      {
        TaskComps[i].Timer = TaskComps[i].ItvTime;
        TaskComps[i].Run = 1;
      }
    }
  }
}

void TaskProcess(void)
{
  for (uint8_t i = 0; i < TASKS_MAX; i++)
  {
    if (TaskComps[i].Run)
    {
      TaskComps[i].TaskHook();
      TaskComps[i].Run = 0;
    }
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim == (&TASK_TIM))
  {
    TaskRemarks();
  }
}
