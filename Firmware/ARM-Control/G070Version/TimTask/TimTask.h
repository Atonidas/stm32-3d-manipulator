#ifndef TIMTASK_H
#define TIMTASK_H

#define TASK_TIM htim6
#define TASKS_MAX 5
// 任务结构
typedef struct _TASK_COMPONENTS
{
    unsigned char Run;            // 程序运行标记：0-不运行，1运行
    unsigned short Timer;         // 计时器
    unsigned short ItvTime;       // 任务运行间隔时间
    void (*TaskHook)(void); // 要运行的任务函数
} TASK_COMPONENTS;          // 任务定义

void TaskProcess(void);

#endif
