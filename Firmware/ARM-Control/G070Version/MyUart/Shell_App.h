#include "MyShell.h"

#define RECORD_IDLE 0
#define RECORD_WORK 1
#define RECORD_END 2
#define RECORD_REPLAY 3

void cmd_Analyze(char *input);

void cmd_Arm_Move(void);
void cmd_Arm_setSpeed(void);
void cmd_Arm_setMode(void);
void cmd_Arm_Claw(void);
void cmd_Arm_Zero(void);
void cmd_Arm_setBeep(void);

void arm_register(void);