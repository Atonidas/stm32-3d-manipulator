#include "MyUart.h"
#include "MyShell.h"
#include "Shell_App.h"
#include "StepMotor.h"

typedef struct
{
    char str[50];
} TAG_Tydef;

const char bank[2] = " ", comma[2] = ",";
volatile char *cmdTemp = NULL;
extern uint8_t ledState, beepState, beepNum, queueFlag, AiClawState;
extern uint16_t claw_Timer;

uint8_t armState = 0, DeviceState, recordPoint = 0, recordNum = 0;
__PonitData_Tydef record_Data[10] = {0};
#define TAG_LENGTH 14

const TAG_Tydef Help_TAG[TAG_LENGTH] =
    {
        "\n\r\n\r+---------------------------------+",
        "\n\r|     机械臂控制指令说明书        |",
        "\n\r|        Version: 1.0.0           |",
        "\n\r+---------------------------------+",
        "\n\r|move x,y,z    |  移动命令        |",
        "\n\r|set-speed x   |  改速度          |",
        "\n\r|set-beep      |  调整蜂鸣器      |",
        "\n\r|set-mode mode |  增量或绝对坐标  |",
        "\n\r|claw t        |  开关爪子t豪秒   |",
        "\n\r|move-zero     |  回原点          |",
        "\n\r|report        |  打印脉冲信息    |",
        "\n\r|record        |  开始或结束录制  |",
        "\n\r|ai-claw       |  自动抓附近物品  |",
        "\n\r+---------------------------------+\n\r",
};

void cmd_Analyze(char *input)
{
    cmdTemp = strtok(input, bank);
    match_cmd((char *)cmdTemp);
}
void Print_HelpTAG(void)
{
    set_Print_Style(F_BLUE, S_LIHGT);
    for (int8_t i = 0; i < TAG_LENGTH; i++)
        Uart1_Printf((char *)Help_TAG[i].str);
    set_Print_Style(F_WHITE, S_NORMAL);
}
void record_OneFrame(__PonitData_Tydef record)
{
    record_Data[recordPoint].X = record.X;
    record_Data[recordPoint].Y = record.Y;
    record_Data[recordPoint].Z = record.Z;
    recordPoint++;
}

void cmd_Arm_Move(void)
{
    int numTemp[3] = {0};
    char *numChar;
    __MotorAngle_Tydef angle;
    __PonitData_Tydef pNow, pTarget;
    for (uint8_t i = 0; i < 3; i++)
    {
        numChar = strtok(NULL, comma);
        numTemp[i] = atoi(numChar);
    }
    pTarget.X = numTemp[0];
    pTarget.Y = numTemp[1];
    pTarget.Z = numTemp[2];
    getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &pNow);
    Frame_By_Frame(pNow, pTarget, motorSpeed);
    if (DeviceState == RECORD_WORK)
    {
        record_OneFrame(pTarget);
    }
    beepNum = 8;
    beepState = 0x01;
    armState = 1;
    queueFlag = 0;
}
void cmd_Arm_setSpeed(void)
{
    Uart1_Printf("set speed is ok\n\n");
}
void cmd_Arm_setMode(void)
{
    Uart1_Printf("arm move is ok\n\n");
}
void cmd_Arm_Claw(void)
{
    int numTemp;
    char *numChar;
    numChar = strtok(NULL, comma);
    numTemp = atoi(numChar);
    if (numTemp > 0)
    {
        HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 0);
        HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 1);
        claw_Timer = numTemp / 10;
        Uart1_Printf("Arm_Claw Open %dms\n\n", numTemp);
    }
    else
    {
        HAL_GPIO_WritePin(MOTO_A_GPIO_Port, MOTO_A_Pin, 0);
        HAL_GPIO_WritePin(MOTO_B_GPIO_Port, MOTO_B_Pin, 1);
        claw_Timer = -numTemp / 10;
        Uart1_Printf("Arm_Claw Close %dms\n\n", -numTemp);
    }
}
void cmd_Arm_Zero(void)
{
    Motor_Zero();
    Uart1_Printf("Arm_Zero is ok\n\n");
}
void cmd_Arm_setBeep(void)
{
    int numTemp[2] = {0};
    char *numChar;
    for (uint8_t i = 0; i < 2; i++)
    {
        numChar = strtok(NULL, comma);
        numTemp[i] = atoi(numChar);
    }
    beepNum = numTemp[0];
    beepState = numTemp[1];
    Uart1_Printf("beep setting compelete\n");
}
void cmd_Arm_report(void)
{
    __PonitData_Tydef P;
    Motor_Print();
    getArmPoint(ArmMotor[0].TotalPulse, ArmMotor[1].TotalPulse, ArmMotor[2].TotalPulse, &P);
    Uart1_Printf("Point: (%d, %d, %d)\n", (int)P.X, (int)P.Y, (int)P.Z);
}

void cmd_Arm_Record(void)
{
    char cmd_temp[20] = {0};
    char *pTemp = &cmdTemp;
    pTemp = strtok(NULL, bank);
    if (!strcmp(pTemp, "start"))
    {
        Uart1_Printf("start record!\n");
        recordPoint = 0;
        DeviceState = RECORD_WORK;
    }
    if (!strcmp(pTemp, "end"))
    {
        DeviceState = RECORD_END;
        recordNum = recordPoint;
        recordPoint = 0;
        Uart1_Printf("record end by %d Point\n", recordNum);
    }
    if (!strcmp(pTemp, "auto"))
    {
        recordPoint = 0;
        DeviceState = RECORD_REPLAY;
        beepNum = 9;
        beepState = 0x05;
        queueFlag = 1;
    }
}

void cmd_start_AIclaw(void)
{
    Uart1_Printf("start auto claw\n");
    AiClawState = 1;
}
void arm_register(void)
{
    //初始化 字符串命令和对应函数
    CMD_Name_Func arm_cmds[] =
        {
            {"move", cmd_Arm_Move},          // 一个结构体变量大小为 12 (字符串大小10 + 函数名大小2)
            {"set-speed", cmd_Arm_setSpeed}, // 一个结构体变量大小为 12
            {"set-mode", cmd_Arm_setMode},
            {"set-beep", cmd_Arm_setBeep},
            {"claw", cmd_Arm_Claw},
            {"move-zero", cmd_Arm_Zero},
            {"record", cmd_Arm_Record},
            {"report", cmd_Arm_report},
            {"ai-claw", cmd_start_AIclaw},
        };
    //将命令添加到列表中
    register_cmds(arm_cmds, ARRAY_SIZE(arm_cmds)); // ARRAY_SIZE 用来计算结构体数组中，数组的个数。个数=结构体总长度/单个数组长度
}