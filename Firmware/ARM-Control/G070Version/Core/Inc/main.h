/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define R_MIN_Pin GPIO_PIN_13
#define R_MIN_GPIO_Port GPIOC
#define L_MIN_Pin GPIO_PIN_14
#define L_MIN_GPIO_Port GPIOC
#define Z_MIN_Pin GPIO_PIN_15
#define Z_MIN_GPIO_Port GPIOC
#define OLED_DC_Pin GPIO_PIN_6
#define OLED_DC_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_8
#define LED_GPIO_Port GPIOA
#define MOTO_A_Pin GPIO_PIN_15
#define MOTO_A_GPIO_Port GPIOA
#define BEEP_Pin GPIO_PIN_3
#define BEEP_GPIO_Port GPIOD
#define MOTO_EN_Pin GPIO_PIN_3
#define MOTO_EN_GPIO_Port GPIOB
#define MOTO_B_Pin GPIO_PIN_5
#define MOTO_B_GPIO_Port GPIOB
#define SRCLR_Pin GPIO_PIN_6
#define SRCLR_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define getLimt_L() HAL_GPIO_ReadPin(L_MIN_GPIO_Port, L_MIN_Pin)
#define getLimt_R() HAL_GPIO_ReadPin(R_MIN_GPIO_Port, R_MIN_Pin)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
