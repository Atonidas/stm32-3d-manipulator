// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.0.5
// LVGL VERSION: 8.2
// PROJECT: SquareLine_Project

#ifndef _SQUARELINE_PROJECT_UI_H
#define _SQUARELINE_PROJECT_UI_H

#ifdef __cplusplus
extern "C" {
#endif

#if __has_include("lvgl.h")
#include "lvgl.h"
#else
#include "lvgl/lvgl.h"
#endif

extern lv_obj_t * ui_Screen1;
extern lv_obj_t * ui_Back;
extern lv_obj_t * ui_img_point;
extern lv_obj_t * ui_Slider3;
extern lv_obj_t * ui_img_aim;
extern lv_obj_t * ui_img_speed;
extern lv_obj_t * ui_Arc2;
extern lv_obj_t * ui_Roller_menu;
extern lv_obj_t * ui_Label1;
extern lv_obj_t * ui_Label2;
extern lv_obj_t * ui_Label3;
extern lv_obj_t * ui_img_link;
extern lv_obj_t * ui_learning;


LV_IMG_DECLARE(ui_img_point_png);    // assets\Point.png
LV_IMG_DECLARE(ui_img_aim_png);    // assets\aim.png
LV_IMG_DECLARE(ui_img_dashboard_png);    // assets\dashboard.png
LV_IMG_DECLARE(ui_img_api_png);    // assets\api.png
LV_IMG_DECLARE(ui_img_534637077);    // assets\play-circle.png




void ui_init(void);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif
