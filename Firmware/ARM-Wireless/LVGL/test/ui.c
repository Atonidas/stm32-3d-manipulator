// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.0.5
// LVGL VERSION: 8.2
// PROJECT: SquareLine_Project

#include "ui.h"
#include "ui_helpers.h"

///////////////////// VARIABLES ////////////////////
lv_obj_t * ui_Screen1;
lv_obj_t * ui_Back;
lv_obj_t * ui_img_point;
lv_obj_t * ui_Slider3;
lv_obj_t * ui_img_aim;
lv_obj_t * ui_img_speed;
lv_obj_t * ui_Arc2;
lv_obj_t * ui_Roller_menu;
lv_obj_t * ui_Label1;
lv_obj_t * ui_Label2;
lv_obj_t * ui_Label3;
lv_obj_t * ui_img_link;
lv_obj_t * ui_learning;

///////////////////// TEST LVGL SETTINGS ////////////////////
#if LV_COLOR_DEPTH != 16
    #error "LV_COLOR_DEPTH should be 16bit to match SquareLine Studio's settings"
#endif
#if LV_COLOR_16_SWAP !=1
    #error "#error LV_COLOR_16_SWAP should be 1 to match SquareLine Studio's settings"
#endif

///////////////////// ANIMATIONS ////////////////////

// ui_EloAnimation0
// FUNCTION HEADER
void linking_Animation(lv_obj_t * TargetObject, int delay);

// FUNCTION
void linking_Animation(lv_obj_t * TargetObject, int delay)
{

    //
    lv_anim_t PropertyAnimation_0;
    lv_anim_init(&PropertyAnimation_0);
    lv_anim_set_time(&PropertyAnimation_0, 1000);
    lv_anim_set_user_data(&PropertyAnimation_0, TargetObject);
    lv_anim_set_custom_exec_cb(&PropertyAnimation_0, _ui_anim_callback_set_opacity);
    lv_anim_set_values(&PropertyAnimation_0, 0, 255);
    lv_anim_set_path_cb(&PropertyAnimation_0, lv_anim_path_ease_in_out);
    lv_anim_set_delay(&PropertyAnimation_0, delay + 0);
    lv_anim_set_early_apply(&PropertyAnimation_0, false);
    lv_anim_set_get_value_cb(&PropertyAnimation_0, &_ui_anim_callback_get_opacity);
    lv_anim_start(&PropertyAnimation_0);

}

// ui_EloAnimation0
// FUNCTION HEADER
void learn_Animation(lv_obj_t * TargetObject, int delay);

// FUNCTION
void learn_Animation(lv_obj_t * TargetObject, int delay)
{

    //
    lv_anim_t PropertyAnimation_0;
    lv_anim_init(&PropertyAnimation_0);
    lv_anim_set_time(&PropertyAnimation_0, 500);
    lv_anim_set_user_data(&PropertyAnimation_0, TargetObject);
    lv_anim_set_custom_exec_cb(&PropertyAnimation_0, _ui_anim_callback_set_opacity);
    lv_anim_set_values(&PropertyAnimation_0, 0, 255);
    lv_anim_set_path_cb(&PropertyAnimation_0, lv_anim_path_ease_in);
    lv_anim_set_delay(&PropertyAnimation_0, delay + 0);
    lv_anim_set_early_apply(&PropertyAnimation_0, false);
    lv_anim_set_get_value_cb(&PropertyAnimation_0, &_ui_anim_callback_get_opacity);
    lv_anim_start(&PropertyAnimation_0);

}

///////////////////// FUNCTIONS ////////////////////

///////////////////// SCREENS ////////////////////
void ui_Screen1_screen_init(void)
{

    // ui_Screen1

    ui_Screen1 = lv_obj_create(NULL);

    lv_obj_clear_flag(ui_Screen1, LV_OBJ_FLAG_SCROLLABLE);

    // ui_Back

    ui_Back = lv_obj_create(ui_Screen1);

    lv_obj_set_width(ui_Back, 288);
    lv_obj_set_height(ui_Back, 246);

    lv_obj_set_x(ui_Back, 0);
    lv_obj_set_y(ui_Back, 0);

    lv_obj_set_align(ui_Back, LV_ALIGN_CENTER);

    lv_obj_clear_flag(ui_Back, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_set_style_radius(ui_Back, 50, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_Back, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Back, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_blend_mode(ui_Back, LV_BLEND_MODE.NORMAL, LV_PART_MAIN | LV_STATE_DEFAULT);

    // ui_img_point

    ui_img_point = lv_img_create(ui_Screen1);
    lv_img_set_src(ui_img_point, &ui_img_point_png);

    lv_obj_set_width(ui_img_point, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_img_point, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_img_point, -62);
    lv_obj_set_y(ui_img_point, 37);

    lv_obj_set_align(ui_img_point, LV_ALIGN_CENTER);

    lv_obj_add_flag(ui_img_point, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_img_point, LV_OBJ_FLAG_SCROLLABLE);

    // ui_Slider3

    ui_Slider3 = lv_slider_create(ui_Screen1);
    lv_slider_set_range(ui_Slider3, 0, 100);
    lv_slider_set_mode(ui_Slider3, LV_SLIDER_MODE_SYMMETRICAL);
    lv_slider_set_value(ui_Slider3, 50, LV_ANIM_OFF);
    if(lv_slider_get_mode(ui_Slider3) == LV_SLIDER_MODE_RANGE) lv_slider_set_left_value(ui_Slider3, 0, LV_ANIM_OFF);

    lv_obj_set_width(ui_Slider3, 6);
    lv_obj_set_height(ui_Slider3, 140);

    lv_obj_set_x(ui_Slider3, 21);
    lv_obj_set_y(ui_Slider3, 41);

    lv_obj_set_align(ui_Slider3, LV_ALIGN_CENTER);

    lv_obj_set_style_bg_color(ui_Slider3, lv_color_hex(0x37A9FF), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Slider3, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(ui_Slider3, lv_color_hex(0x000000), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(ui_Slider3, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_Slider3, lv_color_hex(0x37A9FF), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Slider3, 255, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(ui_Slider3, lv_color_hex(0x000000), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(ui_Slider3, 255, LV_PART_KNOB | LV_STATE_DEFAULT);

    // ui_img_aim

    ui_img_aim = lv_img_create(ui_Screen1);
    lv_img_set_src(ui_img_aim, &ui_img_aim_png);

    lv_obj_set_width(ui_img_aim, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_img_aim, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_img_aim, 63);
    lv_obj_set_y(ui_img_aim, 142);

    lv_obj_add_flag(ui_img_aim, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_img_aim, LV_OBJ_FLAG_SCROLLABLE);

    // ui_img_speed

    ui_img_speed = lv_img_create(ui_Screen1);
    lv_img_set_src(ui_img_speed, &ui_img_dashboard_png);

    lv_obj_set_width(ui_img_speed, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_img_speed, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_img_speed, 84);
    lv_obj_set_y(ui_img_speed, 96);

    lv_obj_set_align(ui_img_speed, LV_ALIGN_CENTER);

    lv_obj_add_flag(ui_img_speed, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_img_speed, LV_OBJ_FLAG_SCROLLABLE);

    // ui_Arc2

    ui_Arc2 = lv_arc_create(ui_Screen1);

    lv_obj_set_width(ui_Arc2, 80);
    lv_obj_set_height(ui_Arc2, 80);

    lv_obj_set_x(ui_Arc2, 183);
    lv_obj_set_y(ui_Arc2, 180);

    lv_arc_set_range(ui_Arc2, 0, 100);
    lv_arc_set_value(ui_Arc2, 50);
    lv_arc_set_bg_angles(ui_Arc2, 180, 0);

    lv_obj_set_style_blend_mode(ui_Arc2, LV_BLEND_MODE.NORMAL, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_color(ui_Arc2, lv_color_hex(0x37A9FF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_Arc2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_Arc2, lv_color_hex(0xEE8D31), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Arc2, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_color(ui_Arc2, lv_color_hex(0xEE8D31), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_Arc2, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_Arc2, lv_color_hex(0xEE8D31), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Arc2, 255, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(ui_Arc2, lv_color_hex(0x37A9FF), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(ui_Arc2, 255, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_width(ui_Arc2, 2, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_spread(ui_Arc2, 2, LV_PART_KNOB | LV_STATE_DEFAULT);

    // ui_Roller_menu

    ui_Roller_menu = lv_roller_create(ui_Screen1);
    lv_roller_set_options(ui_Roller_menu, "move X\nmove Y\nmove Z\ncatch\nspeed\nauto", LV_ROLLER_MODE_INFINITE);

    lv_obj_set_height(ui_Roller_menu, 72);
    lv_obj_set_width(ui_Roller_menu, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_Roller_menu, 173);
    lv_obj_set_y(ui_Roller_menu, 87);

    lv_obj_set_style_text_color(ui_Roller_menu, lv_color_hex(0x808080), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Roller_menu, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Roller_menu, &lv_font_montserrat_14, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_Roller_menu, lv_color_hex(0x001C31), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Roller_menu, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_text_color(ui_Roller_menu, lv_color_hex(0xEE8D31), LV_PART_SELECTED | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Roller_menu, 255, LV_PART_SELECTED | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Roller_menu, &lv_font_montserrat_18, LV_PART_SELECTED | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_Roller_menu, lv_color_hex(0x37A9FF), LV_PART_SELECTED | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Roller_menu, 255, LV_PART_SELECTED | LV_STATE_DEFAULT);

    // ui_Label1

    ui_Label1 = lv_label_create(ui_Screen1);

    lv_obj_set_width(ui_Label1, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_Label1, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_Label1, 10);
    lv_obj_set_y(ui_Label1, 40);

    lv_label_set_text(ui_Label1, "Height : 200mm");

    lv_obj_set_style_text_color(ui_Label1, lv_color_hex(0xEE8D31), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label1, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    // ui_Label2

    ui_Label2 = lv_label_create(ui_Screen1);

    lv_obj_set_width(ui_Label2, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_Label2, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_Label2, 10);
    lv_obj_set_y(ui_Label2, 20);

    lv_label_set_text(ui_Label2, "Speed: 5 mm/s");

    lv_obj_set_style_text_color(ui_Label2, lv_color_hex(0xEE8D31), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    // ui_Label3

    ui_Label3 = lv_label_create(ui_Screen1);

    lv_obj_set_width(ui_Label3, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_Label3, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_Label3, 10);
    lv_obj_set_y(ui_Label3, 60);

    lv_label_set_text(ui_Label3, "Point : (200, 200, 200)");

    lv_obj_set_style_text_color(ui_Label3, lv_color_hex(0xEE8D31), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    // ui_img_link

    ui_img_link = lv_img_create(ui_Screen1);
    lv_img_set_src(ui_img_link, &ui_img_api_png);

    lv_obj_set_width(ui_img_link, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_img_link, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_img_link, 235);
    lv_obj_set_y(ui_img_link, 20);

    lv_obj_add_flag(ui_img_link, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_img_link, LV_OBJ_FLAG_SCROLLABLE);

    // ui_learning

    ui_learning = lv_img_create(ui_Screen1);
    lv_img_set_src(ui_learning, &ui_img_534637077);

    lv_obj_set_width(ui_learning, LV_SIZE_CONTENT);
    lv_obj_set_height(ui_learning, LV_SIZE_CONTENT);

    lv_obj_set_x(ui_learning, 180);
    lv_obj_set_y(ui_learning, 20);

    lv_obj_add_flag(ui_learning, LV_OBJ_FLAG_ADV_HITTEST);
    lv_obj_clear_flag(ui_learning, LV_OBJ_FLAG_SCROLLABLE);

}

void ui_init(void)
{
    lv_disp_t * dispp = lv_disp_get_default();
    lv_theme_t * theme = lv_theme_default_init(dispp, lv_palette_main(LV_PALETTE_BLUE), lv_palette_main(LV_PALETTE_RED),
                                               false, LV_FONT_DEFAULT);
    lv_disp_set_theme(dispp, theme);
    ui_Screen1_screen_init();
    lv_disp_load_scr(ui_Screen1);
}

