#include "ST7789.h"

void ST7789_WriteCmd(u8 data)
{
    LCD_DC_Clr();
    HAL_SPI_Transmit(&ST7789_SPI, &data, 1, 0xFF);
    LCD_DC_Set();
}
void ST7789_WirteByte(uint8_t data)
{
    HAL_SPI_Transmit(&ST7789_SPI, &data, 1, 0xFF);
}
void ST7789_WirteWord(uint16_t data)
{
    ST7789_WirteByte(data >> 8);
	ST7789_WirteByte(data);
}
void ST7789_AdrSet(u16 x1,u16 y1,u16 x2,u16 y2)
{
	LCD_CS_Clr();
	if (SCREEN_DIR == 0)
	{
		ST7789_WriteCmd(ST7789_CASET); //列地址设置
		ST7789_WirteWord(x1 + SCREEN_X_START);
		ST7789_WirteWord(x2 + SCREEN_X_START);
		ST7789_WriteCmd(ST7789_RASET);
		ST7789_WirteWord(y1 + SCREEN_Y_START);
		ST7789_WirteWord(y2 + SCREEN_Y_START);
		ST7789_WriteCmd(ST7789_RAMWR);
	}
	else if (SCREEN_DIR == 1)
	{
		ST7789_WriteCmd(ST7789_CASET); //列地址设置
		ST7789_WirteWord(x1 + SCREEN_X_START);
		ST7789_WirteWord(x2 + SCREEN_X_START);
		ST7789_WriteCmd(ST7789_RASET);
		ST7789_WirteWord(y1 + SCREEN_Y_START);
		ST7789_WirteWord(y2 + SCREEN_Y_START);
		ST7789_WriteCmd(ST7789_RAMWR);
	}
	else if (SCREEN_DIR == 2)
	{
		ST7789_WriteCmd(ST7789_CASET); //列地址设置
		ST7789_WirteWord(x1 + SCREEN_X_START);
		ST7789_WirteWord(x2 + SCREEN_X_START);
		ST7789_WriteCmd(ST7789_RASET);
		ST7789_WirteWord(y1 + SCREEN_Y_START);
		ST7789_WirteWord(y2 + SCREEN_Y_START);
		ST7789_WriteCmd(ST7789_RAMWR);
	}
	else
	{
		ST7789_WriteCmd(ST7789_CASET); //列地址设置
		ST7789_WirteWord(x1 + SCREEN_X_START);
		ST7789_WirteWord(x2 + SCREEN_X_START);
		ST7789_WriteCmd(ST7789_RASET);
		ST7789_WirteWord(y1 + SCREEN_Y_START);
		ST7789_WirteWord(y2 + SCREEN_Y_START);
		ST7789_WriteCmd(ST7789_RAMWR);
	}
	LCD_CS_Set();
}
void ST7789_Init(void)
{
	LCD_CS_Clr();
	//************* Start Initial Sequence **********//
	ST7789_WriteCmd(0x11); // Sleep out
	HAL_Delay(120);	  // Delay 120ms
	//************* Start Initial Sequence **********//
	ST7789_WriteCmd(0x36);
	if (SCREEN_DIR == 0)
		ST7789_WirteByte(0x00);
	else if (SCREEN_DIR == 1)
		ST7789_WirteByte(0xC0);
	else if (SCREEN_DIR == 2)
		ST7789_WirteByte(0x70);
	else
		ST7789_WirteByte(0xA0);

	ST7789_WriteCmd(0x3A);
	ST7789_WirteByte(0x05);

	ST7789_WriteCmd(0xB2);
	ST7789_WirteByte(0x0C);
	ST7789_WirteByte(0x0C);
	ST7789_WirteByte(0x00);
	ST7789_WirteByte(0x33);
	ST7789_WirteByte(0x33);

	ST7789_WriteCmd(0xB7);
	ST7789_WirteByte(0x35);

	ST7789_WriteCmd(0xBB);
	ST7789_WirteByte(0x32); // Vcom=1.35V

	ST7789_WriteCmd(0xC2);
	ST7789_WirteByte(0x01);

	ST7789_WriteCmd(0xC3);
	ST7789_WirteByte(0x15); // GVDD=4.8V  颜色深度

	ST7789_WriteCmd(0xC4);
	ST7789_WirteByte(0x20); // VDV, 0x20:0v

	ST7789_WriteCmd(0xC6);
	ST7789_WirteByte(0x0F); // 0x0F:60Hz

	ST7789_WriteCmd(0xD0);
	ST7789_WirteByte(0xA4);
	ST7789_WirteByte(0xA1);

	ST7789_WriteCmd(0xE0);
	ST7789_WirteByte(0xD0);
	ST7789_WirteByte(0x08);
	ST7789_WirteByte(0x0E);
	ST7789_WirteByte(0x09);
	ST7789_WirteByte(0x09);
	ST7789_WirteByte(0x05);
	ST7789_WirteByte(0x31);
	ST7789_WirteByte(0x33);
	ST7789_WirteByte(0x48);
	ST7789_WirteByte(0x17);
	ST7789_WirteByte(0x14);
	ST7789_WirteByte(0x15);
	ST7789_WirteByte(0x31);
	ST7789_WirteByte(0x34);

	ST7789_WriteCmd(0xE1);
	ST7789_WirteByte(0xD0);
	ST7789_WirteByte(0x08);
	ST7789_WirteByte(0x0E);
	ST7789_WirteByte(0x09);
	ST7789_WirteByte(0x09);
	ST7789_WirteByte(0x15);
	ST7789_WirteByte(0x31);
	ST7789_WirteByte(0x33);
	ST7789_WirteByte(0x48);
	ST7789_WirteByte(0x17);
	ST7789_WirteByte(0x14);
	ST7789_WirteByte(0x15);
	ST7789_WirteByte(0x31);
	ST7789_WirteByte(0x34);
	ST7789_WriteCmd(0x21);

	ST7789_WriteCmd(0x29);
	LCD_CS_Set();
}
void ST7789_DrawRaw(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t *color_map)
{
    if (x2 < x1 || y2 < y1)
		return;
	if (!color_map)
		return;
	uint32_t size = (x2 - x1 + 1) * (y2 - y1 + 1);
	// Column addresses
	ST7789_AdrSet(x1, y1, x2, y2);
	LCD_CS_Set();
	LCD_CS_Clr();
	// HAL_SPI_Transmit(&hspi4, (uint8_t *)color_map, size * 2, 0xFF);
	// HAL_SPI_Transmit_IT(&hspi4, (uint8_t *)color_map, size * 2);
	HAL_SPI_Transmit_DMA(&hspi4, (uint8_t *)color_map, size * 2);
}