#ifndef __ST7789_H
#define __ST7789_H

#include "main.h"

#define TYPE_172 1
#define TYPE_280 2
#define TYPE_320 3
#define SCREEN_TYPE TYPE_280

// #define Use_ResPin 1

#define SCREEN_DIR 3
#if Use_ResPin
#define LCD_RES_Clr()   HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_RESET)//DC
#define LCD_RES_Set()   HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_SET)
#endif

#if (SCREEN_TYPE == 1)
#define SCREEN_WIDTH 172
#define SCREEN_HIGHT 320
#define X_START 34
#define Y_START 0
#elif (SCREEN_TYPE == 2)
#define SCREEN_WIDTH 240
#define SCREEN_HIGHT 280
#define X_START 0
#define Y_START 20
#elif (SCREEN_TYPE == 3)
#define SCREEN_WIDTH 240
#define SCREEN_HIGHT 320
#define X_START 0
#define Y_START 20
#endif

#if SCREEN_DIR==0||SCREEN_DIR==1
#define LCD_W SCREEN_WIDTH
#define LCD_H SCREEN_HIGHT
#define SCREEN_X_START X_START
#define SCREEN_Y_START Y_START
#else
#define LCD_W SCREEN_HIGHT
#define LCD_H SCREEN_WIDTH
#define SCREEN_X_START Y_START
#define SCREEN_Y_START X_START
#endif


#define LCD_DC_Clr()   HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_RESET)//DC
#define LCD_DC_Set()   HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_SET)

#define LCD_CS_Clr()   HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET)//CS
#define LCD_CS_Set()   HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET)

extern DMA_HandleTypeDef hdma1;
extern SPI_HandleTypeDef hspi4;
#define ST7789_SPI hspi4
#define ST7789_CASET        0x2A
#define ST7789_RASET        0x2B
#define ST7789_RAMWR        0x2C

void ST7789_WriteCmd(u8 data);
void ST7789_AdrSet(u16 x1,u16 y1,u16 x2,u16 y2);
void ST7789_Init(void);
void ST7789_DrawRaw(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t *color_map);

#endif