#ifndef __MYENCODER_H
#define MYENCODER_H

#define USE_STM32_HAL
#define MENU_NUM_MAX 6

#define ABS_DISABLE 0
#define ABS_ENABLE 1
#define DATA_REFRESH 1

void Encoder_IT_Task(void);
void Encoder_Procces(void);

typedef struct _ENCODER_COMPONENTS
{
    int data;
    int max;
    int min;
    int step;
    unsigned char refresh;
    unsigned char absLimit;
    void (*EncoderHook)(void); // 要运行的任务函数
} ENCODER_COMPONENTS;          // 任务定义
extern ENCODER_COMPONENTS Encoder[MENU_NUM_MAX+1];

void ui_menu(char menuNun);
void ui_aim_setX(void);
void ui_aim_setY(void);
void ui_aim_setH(void);
void ui_aim_catch(void);
void ui_aim_setSpeed(void);
void ui_aim_auto(void);

#endif // !MYENCODER_H