#include "lvgl.h"
#include "main.h"
#include "MyEncoder.h"
#include "MyUart.h"
#include "math.h"
#include "ui.h"
#include "tim.h"
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "nrf24L01.h"

#define LENTH_MAX 500
#define LENTH_MIN 200

void ui_menu(char menuNun)
{
    lv_roller_set_selected(ui_Roller_menu, menuNun, LV_ANIM_ON);
}
void ui_aim_setX(void)
{
    Encoder[0].max = sqrt((double)(LENTH_MAX * LENTH_MAX - Encoder[1].data * Encoder[1].data));
    Encoder[0].min = sqrt((double)(LENTH_MIN * LENTH_MIN - Encoder[1].data * Encoder[1].data));
    lv_obj_set_x(ui_img_aim, Encoder[0].data * 0.14 + 63);
    lv_label_set_text_fmt(ui_Label3, "Point :(%d,%d,%d)", Encoder[0].data, Encoder[1].data, Encoder[2].data);
    // lv_label_set_text_fmt(ui_Label3, "min %d, max %d", Encoder[0].min, Encoder[0].max);
}
void ui_aim_setY(void)
{
    Encoder[1].max = sqrt((double)(LENTH_MAX * LENTH_MAX - Encoder[0].data * Encoder[0].data));
    Encoder[1].min = sqrt((double)(LENTH_MIN * LENTH_MIN - Encoder[0].data * Encoder[0].data));
    lv_obj_set_y(ui_img_aim, -Encoder[1].data * 0.14 + 142);
    lv_label_set_text_fmt(ui_Label3, "Point :(%d,%d,%d)", Encoder[0].data, Encoder[1].data, Encoder[2].data);
    // lv_label_set_text_fmt(ui_Label3, "min %d, max %d", Encoder[1].min, Encoder[1].max);
}
void ui_aim_setH(void)
{
    lv_slider_set_value(ui_Slider3, Encoder[2].data * 0.4, LV_ANIM_ON);
    lv_label_set_text_fmt(ui_Label3, "Point :(%d,%d,%d)", Encoder[0].data, Encoder[1].data, Encoder[2].data);
}
void ui_aim_catch(void)
{
    if (Encoder[3].data)
    {
        NRF_Printf("claw 3000 ");
        Uart1_Printf("claw 3000 \n");
        BEEP_ON();
        osDelay(100);
        BEEP_OFF();
        osDelay(1000);
    }

    else
    {
        NRF_Printf("claw -3000 ");
        Uart1_Printf("claw -3000 \n");
        BEEP_ON();
        osDelay(100);
        BEEP_OFF();
        osDelay(1000);
    }
    Encoder[3].refresh = 0;
}
void ui_aim_setSpeed(void)
{
    int newStep = (Encoder[4].data > 300) ? 25 : 10;
    Encoder[0].step = newStep;
    Encoder[1].step = newStep;
    Encoder[2].step = newStep;
    lv_label_set_text_fmt(ui_Label2, "Speed: %d mm/s", Encoder[4].data);
    lv_arc_set_value(ui_Arc2, Encoder[4].data / 50 * 12);
}
void ui_aim_auto(void)
{
    osDelay(200);
    if (Encoder[5].data)
    {
        BEEP_ON();
        osDelay(100);
        BEEP_OFF();
        osDelay(300);
        BEEP_ON();
        osDelay(100);
        BEEP_OFF();
        NRF_Printf("record start ");
        Uart1_Printf("learn start\n");
    }

    else
    {
        BEEP_ON();
        osDelay(100);
        BEEP_OFF();
        NRF_Printf("record end ");
        Uart1_Printf("learn end\n");
    }
    osDelay(200);
}
