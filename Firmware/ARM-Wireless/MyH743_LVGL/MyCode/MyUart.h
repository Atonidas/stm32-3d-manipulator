#ifndef __MYUART_H
#define __MYUART_H

#include "usart.h"
#include "string.h"

#define F_BLACK 30
#define F_RED 31
#define F_GREEN 32
#define F_YELLOW 33
#define F_BLUE 34
#define F_MAGENTA 35
#define F_CYAN 36
#define F_WHITE 37
/* output log background color */
#define B_NULL
#define B_BLACK 40
#define B_RED 41
#define B_GREEN 42
#define B_YELLOW 43
#define B_BLUE 44
#define B_MAGENTA 45
#define B_CYAN 46
#define B_WHITE 47
#define S_NORMAL 0
#define S_LIHGT 1
#define S_UNDER 4
#define S_INVERSE 7
#define S_N_LIHGT 22
#define S_N_UNDER 25
#define S_N_INVERSE 27

#define Uart1_Output(data, len)  HAL_UART_Transmit_DMA(&huart1, data, len)
// #define Uart2_Output(data, len)  HAL_UART_Transmit_DMA(&huart2, data, len)

// #define BUFFER_SIZE 100
// extern volatile uint8_t rx_len;        //接收一帧数据的长度
// extern volatile uint8_t recv_end_flag; //一帧数据接收完成标志
// extern uint8_t rx_buffer[100];         //接收数据缓存数组

void set_Print_Style(uint8_t F, uint8_t B);
void Uart1_Printf(char *fmt, ...);
// void Uart2_Printf(char *fmt, ...);
// void MyUart_Iint(void);
// void DMA_Usart_Send(uint8_t *buf, uint8_t len);
// void MyUart1_DMA_task(void);

#endif // ! __MYUART_H
