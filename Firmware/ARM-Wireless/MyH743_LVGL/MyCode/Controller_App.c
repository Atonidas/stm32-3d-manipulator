#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "MyEncoder.h"
#include "tim.h"

#include "MyUart.h"
#include "nrf24L01.h"

#include "string.h"

#define SEND_IDLE 0
#define SEND_LOADING 1
#define SEND_WORKING 2
#define SEND_COMPLETE 3

#define IDLE_TIME 60

extern int menuNum;
uint8_t stateSend = SEND_IDLE;
uint8_t EC11_Refresh = 0, SI24_Refresh = 0;
uint8_t Data_RxBuffer[NRF_BUF_LENGTH] = {0};

void Wireless_Process(void)
{
    static uint16_t tickNum = 0;
    switch (stateSend)
    {
    case SEND_IDLE:
        if (EC11_Refresh)
        {
            tickNum = IDLE_TIME;
            stateSend = SEND_LOADING;
            EC11_Refresh = 0;
        }
        osDelay(200);
        break;
    case SEND_LOADING:
        if (EC11_Refresh)
        {
            tickNum = IDLE_TIME;
            Encoder_Procces();
            EC11_Refresh = 0;
        }
        if (tickNum-- < 1)
        {
            HAL_NVIC_DisableIRQ(EC_A_EXTI_IRQn);
            NRF24L01_TX_Mode(TX_ADDRESS);
            if (menuNum < 3)
            {
                NRF_Printf("move %d,%d,%d", Encoder[0].data, Encoder[1].data, Encoder[2].data);
                BEEP_ON();
                osDelay(100);
                BEEP_OFF();
            }
            Uart1_Printf("load complete\n");
            stateSend = SEND_WORKING;
        }
        osDelay(20);
        break;
    case SEND_WORKING:
        NRF24L01_RX_Mode(RX_ADDRESS);
        if (TxRx_complete)
        {
            TxRx_complete = 0;
            memset(Data_RxBuffer, 0, NRF_BUF_LENGTH);
            NRF24L01_RxPacket(Data_RxBuffer);
            if (strcmp(Data_RxBuffer, "move complete"))
            {
                HAL_NVIC_EnableIRQ(EC_A_EXTI_IRQn);
                stateSend = SEND_IDLE;
            }
        }
        HAL_NVIC_EnableIRQ(EC_A_EXTI_IRQn);
        stateSend = SEND_IDLE;
        osDelay(100);
        break;
    default:
        break;
    }
}