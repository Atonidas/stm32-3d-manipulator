#include "MyEncoder.h"
#include "math.h"

#include "MyUart.h"
int menuNum = 0;

ENCODER_COMPONENTS Encoder[MENU_NUM_MAX + 1] =
    {
        {0, 450, -450, 10, 1, ABS_ENABLE, ui_aim_setX},
        {350, 450, -450, 10, 1, ABS_ENABLE, ui_aim_setY},
        {50, 250, -10, 10, 1, ABS_DISABLE, ui_aim_setH},
        {0, 1, 0, 1, 1, ABS_DISABLE, ui_aim_catch},
        {50, 400, 50, 50, 1, ABS_DISABLE, ui_aim_setSpeed},
        {1, 1, 0, 1, 0, ABS_DISABLE, ui_aim_auto},
};
// ENCODER_COMPONENTS *myEncoder = &Encoder;
#ifdef USE_STM32_HAL

#define EC_READ_PIN() !HAL_GPIO_ReadPin(EC_B_GPIO_Port, EC_B_Pin)
#define EC_READ_KEY() !HAL_GPIO_ReadPin(EC_KEY_GPIO_Port, EC_KEY_Pin)

void Encoder_IT_Task(void)
{
    if (EC_READ_KEY())
    {
        // if (menuNum == 4)
        //     Uart1_Printf("speed %d\n", Encoder[4].data);
        if (!EC_READ_PIN())
        {
            if (menuNum < MENU_NUM_MAX - 1)
                menuNum++;
        }
        else
        {
            if (menuNum > 0)
                menuNum--;
        }
        ui_menu(menuNum);
    }
    else
    {
        if (EC_READ_PIN())
        {
            if (Encoder[menuNum].absLimit)
            {
                if ((abs(Encoder[menuNum].data + Encoder[menuNum].step) <= Encoder[menuNum].max) && (abs(Encoder[menuNum].data + Encoder[menuNum].step) >= Encoder[menuNum].min))
                    Encoder[menuNum].data += Encoder[menuNum].step;
            }
            else if (Encoder[menuNum].data + Encoder[menuNum].step <= Encoder[menuNum].max)
            {
                Encoder[menuNum].data += Encoder[menuNum].step;
            }
        }
        else
        {
            if (Encoder[menuNum].absLimit)
            {
                if ((abs(Encoder[menuNum].data - Encoder[menuNum].step) <= Encoder[menuNum].max) && (abs(Encoder[menuNum].data - Encoder[menuNum].step) >= Encoder[menuNum].min))
                    Encoder[menuNum].data -= Encoder[menuNum].step;
            }
            else if (Encoder[menuNum].data - Encoder[menuNum].step >= Encoder[menuNum].min)
            {
                Encoder[menuNum].data -= Encoder[menuNum].step;
            }
        }
        Encoder[menuNum].refresh = DATA_REFRESH;
    }
}

void Encoder_Procces(void)
{
    for (int i = 0; i < MENU_NUM_MAX; i++)
    {
        if (Encoder[i].refresh)
        {
            Encoder[i].EncoderHook();
            Encoder[i].refresh = 0;
        }
    }
}

#endif // USE_STM32_HAL