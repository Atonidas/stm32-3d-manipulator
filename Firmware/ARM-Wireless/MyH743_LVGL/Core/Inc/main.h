/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define EC_B_Pin GPIO_PIN_1
#define EC_B_GPIO_Port GPIOC
#define EC_KEY_Pin GPIO_PIN_2
#define EC_KEY_GPIO_Port GPIOC
#define EC_A_Pin GPIO_PIN_3
#define EC_A_GPIO_Port GPIOC
#define EC_A_EXTI_IRQn EXTI3_IRQn
#define KEY_Pin GPIO_PIN_0
#define KEY_GPIO_Port GPIOA
#define LCD_DC_Pin GPIO_PIN_10
#define LCD_DC_GPIO_Port GPIOE
#define LCD_CS_Pin GPIO_PIN_11
#define LCD_CS_GPIO_Port GPIOE
#define SI24_CE_Pin GPIO_PIN_8
#define SI24_CE_GPIO_Port GPIOC
#define SI24_CS_Pin GPIO_PIN_8
#define SI24_CS_GPIO_Port GPIOA
#define SI24_IRQ_Pin GPIO_PIN_1
#define SI24_IRQ_GPIO_Port GPIOD
#define SI24_IRQ_EXTI_IRQn EXTI1_IRQn
/* USER CODE BEGIN Private defines */
#define u8  uint8_t
#define u16 uint16_t
#define u32 uint32_t

#define BEEP_ON() HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1)
#define BEEP_OFF() HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
