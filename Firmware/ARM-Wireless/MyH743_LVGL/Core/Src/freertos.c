/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tim.h"
#include "lvgl.h"
#include "lv_port_disp.h"
#include "ui.h"
#include "MyUart.h"
// #include <stdio.h>
// #include "mpu6050.h"
// #include "delay.h"
// #include "inv_mpu.h"
// #include "inv_mpu_dmp_motion_driver.h"
#include "Controller_api.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
// float pitch, roll, yaw;    //欧拉角
// short aacx, aacy, aacz;    //加速度传感器原始数据
// short gyrox, gyroy, gyroz; //陀螺仪原始数据
// short temp;                //温度
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
    .name = "defaultTask",
    .stack_size = 1024 * 4,
    .priority = (osPriority_t)osPriorityNormal,
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
const osThreadAttr_t myTask02_attributes = {
    .name = "myTask02",
    .stack_size = 512 * 4,
    .priority = (osPriority_t)osPriorityLow,
};
/* Definitions for myTask03 */
osThreadId_t myTask03Handle;
const osThreadAttr_t myTask03_attributes = {
    .name = "myTask03",
    .stack_size = 256 * 4,
    .priority = (osPriority_t)osPriorityLow,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void Task_lvgl(void *argument);
void Task_Controller(void *argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void)
{
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(Task_lvgl, NULL, &myTask02_attributes);

  /* creation of myTask03 */
  myTask03Handle = osThreadNew(Task_Controller, NULL, &myTask03_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */
}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartDefaultTask */
  uint8_t flag = 0;
  osDelay(200);
  __HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 500);
  while (NRF24L01_Check())
  {
    osDelay(50);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  }
  NRF24L01_TX_Mode(TX_ADDRESS);
  Uart1_Printf("nrf init is ok\n");
  BEEP_ON();
  osDelay(100);
  BEEP_OFF();
  /* Infinite loop */
  for (;;)
  {
    Encoder_Procces();
    osDelay(16);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_Task_lvgl */
/**
 * @brief Function implementing the myTask02 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_lvgl */
void Task_lvgl(void *argument)
{
  /* USER CODE BEGIN Task_lvgl */
  lv_init();
  lv_port_disp_init();
  // lv_example_meter_1();

  ui_init();

  /* Infinite loop */
  for (;;)
  {
    lv_task_handler();
    osDelay(1);
  }
  /* USER CODE END Task_lvgl */
}

/* USER CODE BEGIN Header_Task_Controller */
/**
 * @brief Function implementing the myTask03 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Task_Controller */
void Task_Controller(void *argument)
{
  /* USER CODE BEGIN Task_Controller */
  int16_t tick = 0;
  for (;;)
  {
    if (HAL_GPIO_ReadPin(KEY_GPIO_Port, KEY_Pin))
    {
      tick = 0;
      while (HAL_GPIO_ReadPin(KEY_GPIO_Port, KEY_Pin))
      {
        tick++;
        osDelay(10);
      }
      if (tick < 20)
      {
        Uart1_Printf("Auto!\n");
        NRF24L01_TX_Mode(TX_ADDRESS);
        NRF_Printf("record auto ");
        BEEP_ON();
        osDelay(50);
        BEEP_OFF();
      }
      else
      {
        Uart1_Printf("ai-claw \n");
        NRF24L01_TX_Mode(TX_ADDRESS);
        NRF_Printf("ai-claw ");
        BEEP_ON();
        osDelay(200);
        BEEP_OFF();
      }
    }
    Wireless_Process();
    osDelay(10);
  }
  /* USER CODE END Task_Controller */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if (GPIO_Pin == EC_A_Pin)
  {
    EC11_Refresh = 1;
    Encoder_IT_Task();
  }
  if (GPIO_Pin == SI24_IRQ_Pin)
  {
    SI24_Refresh = 1;
  }
}
/* USER CODE END Application */
